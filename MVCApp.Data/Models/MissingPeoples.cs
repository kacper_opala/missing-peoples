﻿using System.ComponentModel.DataAnnotations;

namespace MVCApp.Data.Models
{
    public class MissingPeoples
    {
        public int Id { get; set; }

        [Required, Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required, Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Voivodeship")]
        public VoivodeshipName Voivodeship { get; set; }
    }
}
