﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCApp.Data.Models
{
    public class UserData
    {
        public string Id { get; set; }

        [Required, Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required, Display(Name = "User name")]
        public string UserName { get; set; }

        [Required, Display(Name = "Role")]
        public string UserRole { get; set; }

        public List<string> Roles { get; set; }
    }
}
