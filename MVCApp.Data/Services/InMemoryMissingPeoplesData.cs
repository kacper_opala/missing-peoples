﻿using MVCApp.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace MVCApp.Data.Services
{
    public class InMemoryMissingPeoplesData : IMissingPeoplesData
    {
        List<MissingPeoples> missingPeoples;

        public InMemoryMissingPeoplesData()
        {
            missingPeoples = new List<MissingPeoples>()
            {
                new MissingPeoples{ Id = 1, FirstName = "Kacper", LastName = "Opala", Voivodeship = VoivodeshipName.swietokrzyskie},
                new MissingPeoples{ Id = 2, FirstName = "Arek", LastName = "Majak", Voivodeship = VoivodeshipName.mazowieckie},
                new MissingPeoples{ Id = 3, FirstName = "Grzegorz", LastName = "Furga", Voivodeship = VoivodeshipName.mazowieckie},
            };
        }

        public void Add(MissingPeoples missingpeoples)
        {
            missingPeoples.Add(missingpeoples);
            missingpeoples.Id = missingPeoples.Max(r => r.Id) + 1;
        }

        public void Delete(int id)
        {
            var missingpeoples = Get(id);
            if(missingpeoples != null)
            {
                missingPeoples.Remove(missingpeoples);
            }
        }

        public MissingPeoples Get(int id)
        {
            return missingPeoples.FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<MissingPeoples> GetAll()
        {
            return missingPeoples.OrderBy(r => r.Id);
        }

        public void Update(MissingPeoples missingPeoples)
        {
            var existing = Get(missingPeoples.Id);
            if(existing != null)
            {
                existing.FirstName = missingPeoples.FirstName;
                existing.LastName = missingPeoples.LastName;
                existing.Voivodeship = missingPeoples.Voivodeship;
            }
        }
    }
}
