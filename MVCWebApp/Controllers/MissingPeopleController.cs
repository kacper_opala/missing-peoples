﻿using MVCApp.Data.Models;
using MVCApp.Data.Services;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;

namespace MVCWebApp.Controllers
{
    public class MissingPeopleController : Controller
    {
        private readonly IMissingPeoplesData db;

        public MissingPeopleController(IMissingPeoplesData db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult Index(string sortOrder, string currentFilter, string searchStr, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NSortParm = String.IsNullOrEmpty(sortOrder) ? "First_Name_desc" : "";
            ViewBag.LSortParm = sortOrder == "L_Name" ? "Last_Name_desc" : "L_Name";


            if (searchStr != null)
            {
                page = 1;
            }
            else
            {
                searchStr = currentFilter;
            }

            ViewBag.CurrentFilter = searchStr;

            var peoples = from p in db.GetAll()
                          select p;

            if (!String.IsNullOrEmpty(searchStr))
            {
                peoples = peoples.Where(s => s.FirstName.ToUpper().Contains(searchStr.ToUpper())
                                       || s.LastName.ToUpper().Contains(searchStr.ToUpper()));
            }

            switch (sortOrder)
            {
                case "First_Name_desc":
                    peoples = peoples.OrderByDescending(s => s.FirstName);
                    break;
                case "L_Name":
                    peoples = peoples.OrderBy(s => s.LastName);
                    break;
                case "Last_Name_desc":
                    peoples = peoples.OrderByDescending(s => s.LastName);
                    break;
                default:
                    peoples = peoples.OrderBy(s => s.FirstName);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);

            return View(peoples.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var model = db.Get(id);
            if(model == null)
            {
                return View("NotFound");
            }
            return View(model);
        } 

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MissingPeoples missingPeoples)
        {
            //if (String.IsNullOrEmpty(missingPeoples.FirstName))
            //{
            //    ModelState.AddModelError(nameof(missingPeoples.FirstName), "First Name is required!");
            //}

            //if (String.IsNullOrEmpty(missingPeoples.LastName))
            //{
            //    ModelState.AddModelError(nameof(missingPeoples.LastName), "Last Name is required!");
            //}

            if (ModelState.IsValid)
            {
                db.Add(missingPeoples);
                return RedirectToAction("Details", new { id = missingPeoples.Id });
            }
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Edit(int id)
        {
            var model = db.Get(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Edit(MissingPeoples missingPeoples)
        {
            if (ModelState.IsValid)
            {
                db.Update(missingPeoples);
                TempData["Message"] = "You have succesfully saved the data!";
                return RedirectToAction("Details", new { id = missingPeoples.Id });
            }
            return View(missingPeoples);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Delete(int id)
        {
            var model = db.Get(id);
            if(model == null)
            {
                return View("NotFound");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Delete(int id, FormCollection form)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }
    }
}